package db

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"

	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/models"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/pkg/logging"
	test_helpers "gitlab.com/bitscorp/segmed/test-task-go-segmed/pkg/test_helpers"
)

const testingUserID = "user-1"
const testingImageURL1 = "https://www.example.com/image1.jpg"
const testingImageURL2 = "https://www.example.com/image2.jpg"

type serviceRedisSuite struct {
	test_helpers.RedisSuite

	logger *zap.SugaredLogger
}

func TestRedisEntrypointSuite(t *testing.T) {
	handler := &serviceRedisSuite{
		RedisSuite: test_helpers.NewDefaultRedisSuite(t, "test_images"),
		logger:     zaptest.NewLogger(t).Sugar(),
	}

	suite.Run(t, handler)
}

func (s serviceRedisSuite) TestImagesPin() {
	var err error

	l, err := logging.ConfigForEnv("test").Build(
		zap.Fields(zap.String("project", "modules/images")),
	)
	s.Require().Nil(err)

	defer l.Sync()
	logger := l.Sugar()

	repo := NewRedisRepository(s.Conn, logger)

	image1 := models.Image{
		URL: testingImageURL1,
		ID:  1,
	}
	err = repo.ImagesPin(context.Background(), testingUserID, &image1)
	s.Require().Nil(err)

	images, err := repo.ImagesPinned(context.Background(), testingUserID)
	s.Require().Nil(err)
	s.Require().Len(images, 1)
	s.Require().Equal(testingImageURL1, images[0].URL)

	image2 := models.Image{
		URL: testingImageURL2,
		ID:  2,
	}
	err = repo.ImagesPin(context.Background(), testingUserID, &image2)
	s.Require().Nil(err)

	images, err = repo.ImagesPinned(context.Background(), testingUserID)
	s.Require().Nil(err)
	s.Require().Len(images, 2)
	s.Require().Equal(testingImageURL1, images[0].URL)
	s.Require().Equal(int64(1), images[0].ID)
	s.Require().Equal(testingImageURL2, images[1].URL)
	s.Require().Equal(int64(2), images[1].ID)
}

func (s serviceRedisSuite) TestImagesList() {
	var err error

	l, err := logging.ConfigForEnv("test").Build(
		zap.Fields(zap.String("project", "modules/images")),
	)
	s.Require().Nil(err)

	defer l.Sync()
	logger := l.Sugar()

	repo := NewRedisRepository(s.Conn, logger)

	images, err := repo.ImagesPinned(context.Background(), testingUserID)
	s.Require().Nil(err)
	s.Require().Len(images, 0)
}

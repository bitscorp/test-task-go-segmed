import { v4 as uuidv4 } from 'uuid';

const User = (storage) => {
  let id = storage.get('users:id');

  if (typeof id === 'undefined' || id === '' || id === null) {
    id = uuidv4();

    storage.set('users:id', id);
  }

  return { id };
};

export default User;

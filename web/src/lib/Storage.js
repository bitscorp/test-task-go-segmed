const Storage = (namespace) => {
  const set = (key, value) => {
    localStorage.setItem(`${namespace}:${key}`, value);
  };

  const get = (key) => localStorage.getItem(`${namespace}:${key}`);

  return { set, get };
};

export default Storage;

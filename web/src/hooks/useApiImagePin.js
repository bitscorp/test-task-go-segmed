import { ref } from 'vue';

import config from '../config/config';

export default function useApiImagePin() {
  const data = ref([]);
  const status = ref('');

  async function pin(userID, image) {
    status.value = 'loading';

    // ensure likes to be as integer value.
    const attrs = JSON.stringify({
      ...image,
      likes: parseInt(image.likes, 10),
      tags: image.tags.join(','),
    });

    try {
      const res = await fetch(`${config.BaseURL}/api/v1/users/${userID}/images/pin`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: attrs,
      });
      const json = await res.json();
      if (res.ok) {
        status.value = 'success';
      } else {
        status.value = 'error';
      }
      data.value = json;
    } catch (e) {
      status.value = 'error';
    }
  }

  return {
    data,
    pin,
    status,
  };
}
